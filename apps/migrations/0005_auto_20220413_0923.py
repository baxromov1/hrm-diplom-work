# Generated by Django 2.2.13 on 2022-04-13 09:23

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('apps', '0004_auto_20220413_0842'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attendance',
            name='status',
            field=models.CharField(
                choices=[('PRESENT', 'ПРИСУТСТВУЕТ'), ('ABSENT', 'ОТСУТСТВУЕТ'), ('UNAVAILABLE', 'НЕДОСТУПЕН')],
                max_length=15),
        ),
        migrations.AlterField(
            model_name='employee',
            name='emp_id',
            field=models.CharField(default='emp334', max_length=70),
        ),
    ]
